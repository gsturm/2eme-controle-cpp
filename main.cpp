#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;
//definition de la structure de la partie
struct PartieMastermind
{
    int combinaisonMystere[4]; //combinaison a trouver
    bool combinaisonTrouver; //vrai quand le joueur trouve la combinaison mystere
    int nbEssaiRestant;// correspond au nombre de chance qui lui reste
    int combinaisonEssayer[4];// combinaison du joueur pour ce tour
    int partieGagner=0;
    int partiePerdu=0;
    int historiqueCombinaison[10][4];//combinaison choisit pour chacun des essais
    int historiqueResultat[10][4];//resultat de tous les essaies
};

//initialisation de la partie
PartieMastermind InitPartie(PartieMastermind partieEnCour)
{
    partieEnCour.nbEssaiRestant = 10;
    partieEnCour.combinaisonTrouver = 0;

    return partieEnCour;
}
//generation de la combinaison aleatoire
PartieMastermind combinaisonAleatoire(PartieMastermind partieEnCour)
{
    int i;
    //remplissage des 4 numeros entre 0 et 9
    for (i=0;i<4;i++)
    {
        partieEnCour.combinaisonMystere[i] = rand()%10;
    }

    return partieEnCour;
}
// affiche la combinaison � trouver
void affichageCombi(int combinaison[4])
{
    int i;
    for(i=0;i<4;i++)
    {
        cout << combinaison[i];
    }
}
// reset de la combinaison tester
PartieMastermind reinitialiseCombinaison(PartieMastermind partieEnCour)
{
    int i;
    for (i=0;i<4;i++)
    {
        partieEnCour.combinaisonEssayer[i] = 10;
    }
    return partieEnCour;
}

// le joueur dit qu'elle est la combinaison qu'il essaie
PartieMastermind demandeCombinaison(PartieMastermind partieEnCour)
{
    int i;
    // on demande les 4 chiffres
    for (i=0;i<4;i++)
    {
        if(i==0)
        {
            cout << "Rentrer le 1er chiffre de votre combinaison :" ;
        }
        else
        {
            cout << "Rentrer le " << i+1 << "eme chiffres de votre combinaison :" ;
        }
        while ((partieEnCour.combinaisonEssayer[i]<0)||(partieEnCour.combinaisonEssayer[i]>9))
        {
            cin >> partieEnCour.combinaisonEssayer[i];
            if((partieEnCour.combinaisonEssayer[i]<0)||(partieEnCour.combinaisonEssayer[i]>9))
            {
                cout << "Rentrer un nombre entre 0 et 9 !!!";
            }
        }
    }
    // enregistrement de la combinaison dans l'historique
    for (i=0;i<4;i++)
    {
        partieEnCour.historiqueCombinaison[10-partieEnCour.nbEssaiRestant][i] = partieEnCour.combinaisonEssayer[i];
    }
    return partieEnCour;
}

//transforme le tableau de resultat en phrase
void affichageResultat(int resultat[4])
{
    int bienPlacer = 0;
    int malPlacer = 0;
    int i;
    for (i=0; i<4; i++)
    {
        if (resultat[i]==2)
        {
            bienPlacer++;
        }
        else if(resultat[i]==1)
        {
            malPlacer++;
        }
    }
    cout << "Il y a " << bienPlacer << " nombre bien placer, et " << malPlacer << " nombre mal placer dans la combinaison!";
}
//verification de la combinaison propos�
PartieMastermind verificationCombinaison(PartieMastermind partieEnCour)
{
    int bienPlacer =0;
    int i,j;
    int resultat[4];//0 si non existant / 1 pour existant mais mal placer / 2 pour bien placer
    // verification de la combinaison
    for (i=0;i<4;i++)
    {
        resultat[i]=0;
        //si le nombre est bien placer
        if(partieEnCour.combinaisonEssayer[i]==partieEnCour.combinaisonMystere[i])
        {
            resultat[i]=2;
        }
        else
        {
            //on parcourt tous les nombres de la combinaison a trouver pour voir si il est mal plac�
            for (j=0;j<4;j++)
            {
                if(partieEnCour.combinaisonEssayer[i]==partieEnCour.combinaisonMystere[j])
                {
                    resultat[i]=1;
                }
            }
        }
    }
    // Enregistrement du resultat dans l'historique
    for (i=0;i<4;i++)
    {
        partieEnCour.historiqueResultat[10-partieEnCour.nbEssaiRestant][i]=resultat[i];
    }
    //verification de victoire
    for (i=0;i<4;i++)
    {
        if (resultat[i]==2)
        {
            bienPlacer++;
            partieEnCour.combinaisonTrouver=0;
        }
    }
    if (bienPlacer==4)
    {
        partieEnCour.combinaisonTrouver=1;
    }
    cout << endl;
    affichageResultat(resultat);
    cout << endl;
    return partieEnCour;

}
//affichage des combinaisons proposer jusqu'a maintenant
void affichageHistorique(PartieMastermind partieEnCour)
{
    int i;
    for (i=0;i<10-partieEnCour.nbEssaiRestant;i++)
    {
        cout << "Essaie numero :" << (i+1) << "     ";
        affichageCombi(partieEnCour.historiqueCombinaison[i]);
        cout << "          ";
        affichageResultat(partieEnCour.historiqueResultat[i]);
        cout << endl;
    }
    cout << endl;
}

int main()
{
    // initialisation du random
    srand(time(NULL));
    // definition et initialisation de la structure de la partie
    int choixRejouer=0;
    int i;
    bool partieContinu = 1;
    PartieMastermind partieEnCour;

    cout << "La partie commence, le but du jeu est de trouver la combinaison de 4 chiffres de l'ordinateur." << endl << "Ces chiffres sont entre 0 et 9.";
    cout << endl << "Pour vous aider, apres chacun de vos essais, l'ordinateur vous dira si vos chiffre existe dans la combinaison, et si ils sont bien placer." << endl;
    //debut de la partie, continue tant que le joueur veux jouer
    while(partieContinu == 1)
    {
        // Generation de la combinaison a trouver pour le tour � venir
        partieEnCour = combinaisonAleatoire(partieEnCour);
        partieEnCour = InitPartie(partieEnCour);
        cout << endl << endl << "l'ordi a choisit un numeros, essayer de le trouver !" << endl ;
        //debut d'un tour de jeu: continue tant que le nb d'essaie est inferieur ou egale � 10, ou que la combinaison est trouv�
        while((partieEnCour.nbEssaiRestant>0)&&(partieEnCour.combinaisonTrouver==0))
        {
            //affichage de tous les essaies et resultat depuis le debut
            affichageHistorique(partieEnCour);
            // reset de la dernier combinaison
            partieEnCour = reinitialiseCombinaison(partieEnCour);
            partieEnCour = demandeCombinaison(partieEnCour);
            partieEnCour = verificationCombinaison(partieEnCour);
            // decremente une chance
            partieEnCour.nbEssaiRestant--;
            //si la combinaison n'est pas trouver et qui reste des chances on affiche combien d'essai il reste
            if((partieEnCour.combinaisonTrouver==0)&&(partieEnCour.nbEssaiRestant>0))
            {
                cout << endl << "Il ne vous reste plus que " << partieEnCour.nbEssaiRestant << " essai..." << endl;
            }
        }
        //message Gagn� ou perdu
        if(partieEnCour.combinaisonTrouver==1)
        {
            cout << endl << "La combinaison a trouver etait :" ;
            affichageCombi(partieEnCour.combinaisonMystere);
            cout<< endl << "Vous avez reussi a trouver le resultat en " << 10-partieEnCour.nbEssaiRestant <<  " tentative, Felicitation !" ;
            partieEnCour.partieGagner++;
        }
        else
        {
            cout << "Vous n'avez pas reussi a trouver la combinaison... La partie est perdu" << endl << "la bonne combinaison etait :" ;
            affichageCombi(partieEnCour.combinaisonMystere);
            partieEnCour.partiePerdu++;
        }
        //affichage des scores
        cout << endl << endl << "Total des scores :" << endl << "Partie Gagner: " << partieEnCour.partieGagner << endl << "Partie Perdu: " << partieEnCour.partiePerdu << endl;
        //Demande de nouvelle partie
        cout << endl << "Voulez-vous encore jouer ?? 1 pour Oui, 0 pour Non :";
        cin >> choixRejouer;
        while((choixRejouer<0)||(choixRejouer>1))
        {
            cout << " 1 pour Oui, 0 pour Non :" << endl;
            cin >> choixRejouer;
        }
        partieContinu = choixRejouer;
    }
}
